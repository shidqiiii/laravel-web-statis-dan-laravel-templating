@extends('layouts.master')

@section('title')
Halaman Index
@endsection

@section('content')
<h1>Selamat Datang! {{$fname}} {{$lname}}</h1>
<p style="font-weight: bold">
  Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!
</p>
@endsection