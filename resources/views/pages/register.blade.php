@extends('layouts.master')

@section('title')
Halaman Form
@endsection

@section('content')
<div class="Footer">
  <h1>Buat Account Baru</h1>
  <h3>Sign Up Form</h3>
</div>

<form method="post" action="/welcome">
  @csrf
  <div class="text">
    <p>First name :</p>
    <input type="text" name="fname" required />
    <p>Last name :</p>
    <input type="text" name="lname" required />
  </div> <br>

  <div class="checkbox">
    <p>Gender</p>
    <input type="radio" value="male" name="gender" required /> Male <br />
    <input type="radio" value="female" name="gender" /> Female
  </div><br>

  <div class="dropdown">
    <p>Nationality</p>
    <select name="nationality" required>
      <option value="" selected>None</option>
      <option value="indonesia">Indonesia</option>
      <option value="amerika">Amerika</option>
      <option value="inggris">Inggris</option>
    </select>
  </div><br>

  <div class="checkbox">
    <p>Language Spoken</p>
    <input type="checkbox" value="indonesia" name="languagespoken" />Bahasa Indonesia<br />
    <input type="checkbox" value="english" name="languagespoken" />English<br />
    <input type="checkbox" value="other" name="languagespoken" />Other<br />
  </div><br>

  <div class="textarea">
    <p>Bio</p>
    <textarea rows="10" cols="30" required></textarea>
  </div><br>

  <input type="submit" value="Sign Up" />
</form>
@endsection