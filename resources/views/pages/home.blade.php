@extends('layouts.master')

@section('title')
Halaman Home
@endsection

@section('content')
<div class="Footer">
  <h1>Media Online</h1>
  <h3>Sosial Media Developer</h3>
  <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
</div>

<div class="Unorder">
  <h3>Benefit join di Media Online</h3>
  <ul>
    <li>Mendapatkan motivasi dari sesama para Developer</li>
    <li>Sharing knowledge</li>
    <li>Dibuat oleh calon web developer terbaik</li>
  </ul>
</div>

<div class="Order">
  <h3>Cara Bergabung ke Media Online</h3>
  <ol>
    <li>Mengunjungi Website ini</li>
    <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
    <li>Selesai</li>
  </ol>
</div>
@endsection