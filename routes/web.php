<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@welcome');

Route::get('/data-tables', 'IndexController@table');

//CRUD Cast

//Create
Route::get('/cast/create', 'CastController@create'); //Create Data
Route::post('/cast', 'CastController@store'); //Store Data

//Read
Route::get('/cast', 'CastController@index'); //Read Data
Route::get('/cast/{cast_id}', 'CastController@show'); //Read Data with detail id

//Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //Edit Data
Route::put('/cast/{cast_id}', 'CastController@update'); //Store Data

//Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy'); //Delete Data